package com.hao.chapter06;

import java.sql.Timestamp;

public class UrlViewCount {
    public String url; //url
    public Long count; //数量
    public Long windowStart; //开始时间
    public Long windowEnd; //结束时间

    public UrlViewCount() {
    }

    public UrlViewCount(String url, Long count, Long windowStart, Long windowEnd) {
        this.url = url;
        this.count = count;
        this.windowStart = windowStart;
        this.windowEnd = windowEnd;
    }

    @Override
    public String toString() {
        return "UrlViewCount[" +
                "url='" + url + '\'' +
                ", count=" + count +
                ", windowStart=" + new Timestamp(windowStart) +
                ", windowEnd=" + new Timestamp(windowEnd) +
                ']';
    }
}
