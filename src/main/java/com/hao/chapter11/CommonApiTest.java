package com.hao.chapter11;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.bridge.java.BatchTableEnvironment;

public class CommonApiTest {
    public static void main(String[] args) {

        //1.定义流环境配置来创建表执行环境
        EnvironmentSettings streamSettings = EnvironmentSettings.newInstance()
                .inStreamingMode() //流环境
                .useBlinkPlanner()
                .build();

        TableEnvironment.create(streamSettings);

        //2.定义批环境配置来创建表执行环境(blink版本planner)
        EnvironmentSettings batchSetting = EnvironmentSettings.newInstance()
                .inBatchMode() //批处理环境
                .useBlinkPlanner()
                .build();

        TableEnvironment.create(batchSetting);

        //3.基于老版本的planner进行流处理
        EnvironmentSettings oldStreamSettings = EnvironmentSettings.newInstance()
                .inStreamingMode() //流处理环境
                .useOldPlanner()
                .build();

        TableEnvironment.create(oldStreamSettings);

        //4.基于老版本的planner进行批处理 (老版本还没有真正意义上的批流通一)
        ExecutionEnvironment batchEnv = ExecutionEnvironment.getExecutionEnvironment();
        BatchTableEnvironment batchTableEnvironment = BatchTableEnvironment.create(batchEnv);

    }
}
