package com.hao.chapter11;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;

public class Udf_Hive {
    public static void main(String[] args) {
        EnvironmentSettings settings = EnvironmentSettings.newInstance()
                .inStreamingMode()
                .useBlinkPlanner()
                .build();
        TableEnvironment tableEnv = TableEnvironment.create(settings);

        String name = "myhive";
        String defaultDatabase = "mydatabase";
        String hiveConfDir = "/opt/hive-conf";

        // 创建一个 HiveCatalog，并在表环境中注册
        HiveCatalog hive = new HiveCatalog(name, defaultDatabase, hiveConfDir);
        tableEnv.registerCatalog("myhive", hive);

        // 使用 HiveCatalog 作为当前会话的 catalog
        tableEnv.useCatalog("myhive");

        // 配置 hive 方言
        tableEnv.getConfig().setSqlDialect(SqlDialect.HIVE);
        // 配置 default 方言
        tableEnv.getConfig().setSqlDialect(SqlDialect.DEFAULT);
    }
}
