package com.hao.chapter05;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class HDFSSourceTest {
    public static void main(String[] args) throws Exception{
        //创建执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        //读取hdfs文件路径
        DataStreamSource<String> hdfsSource = env.readTextFile("hdfs://hadoop102:8020/input/README.txt");
        //将hdfs文件路径打印输出
        hdfsSource.print();
        //执行
        env.execute("HDFSSourceTest");
    }
}
