# 创建flink数据库
create database flink;

# 进入flink数据库
use flink;

# 创建表
create table clicks(
user varchar(20) not null,
url varchar(100) not null);
